#!/usr/bin/env python3

import time

from kuberesources import Kind
from kubernetes import config, client


def start_kind_cluster():
    cluster = Kind()

    # Returns the context of the newly created kind cluster
    cluster.start()

    while not cluster.is_ready():
        print("Waiting for kind to start")
        time.sleep(5)

    print("Cluster is ready now")
    print("This is the context config:", cluster.context_config())

    return cluster

cluster = start_kind_cluster()

context = cluster.context_config()

config.load_kube_config(context=context["context"])

v1 = client.CoreV1Api()
print("Listing pods with their IPs:")
ret = v1.list_namespaced_pod(namespace="kube-system")
for i in ret.items:
    print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))


print("Everything done, removing cluster")
cluster.delete_cluster()
