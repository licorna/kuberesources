#!/usr/bin/env python3

import base64
import json
import os
import urllib.request
import stat
import subprocess
import sys
import tempfile
import time
import yaml


class Kind:
    """Provides a Kind Kubernetes cluster"""
    def __init__(self, name="kind"):
        self.name = name
        self.kind_binary = "kind"
        self.cluster_config_file = None
        
        if not self.binary_exists():
            print("Downloading kind")
            self.download_binary()

    def start(self):
        self.external_process = self.create_cluster()

    def is_ready(self):
        """Checks if the process running kind create is still running"""
        return self.external_process.poll() is not None

    def context_config(self):
        return {
            "context": "kind-" + self.name,
        }

    def configure_ecr(self, registry):
        """Returns a user and password to use with docker login."""
        cmd = ["docker-credential-ecr-login", "get"]
        result = subprocess.run(cmd, input=str.encode(registry, encoding="ascii"), stdout=subprocess.PIPE)
        result_json = json.loads(result.stdout)
        return result_json["Username"], result_json["Secret"]

    def docker_config(self, registry):
        user, secret = self.configure_ecr(registry)
        encoded = str.encode("{}:{}".format(user, secret), encoding="ascii")
        auth = base64.b64encode(encoded).decode("utf-8")

        config = {"auths": {registry: {"auth": auth}}}
        return json.dumps(config)

    def config_private_repo(self, registry):
        """Creates a private repo docker config and the kind cluster config to use it."""
        docker_config = self.docker_config(registry)
        config_file = tempfile.NamedTemporaryFile().name

        with open(config_file, "w") as fd:
            fd.write(docker_config)

        cluster_config = {
            "kind": "Cluster",
            "apiVersion": "kind.sigs.k8s.io/v1alpha3",
            "nodes": [{"role": "control-plane", "extraMounts": [{"containerPath": "/var/lib/kubelet/config.json", "hostPath": config_file}]}]
        }

        cluster_config_file = tempfile.NamedTemporaryFile().name
        with open(cluster_config_file, "w") as fd:
            fd.write(yaml.dump(cluster_config))

        self.cluster_config_file = cluster_config_file

    def create_cluster(self, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL):
        cmd = [self.kind_binary, "create", "cluster"]
        if self.cluster_config_file:
            cmd.append("--config")
            cmd.append(self.cluster_config_file)

        return subprocess.Popen(cmd, stderr=stderr, stdout=stdout)

    def delete_cluster(self, capture_output=True):
        if self.is_ready():
            subprocess.run([self.kind_binary, "delete", "cluster"], capture_output=capture_output)
        else:
            print("Can't delete a cluster that has not finished starting.")

    def binary_exists(self):
        try:
            subprocess.run(["kind"], capture_output=True)
        except FileNotFoundError:
            return False

        return True

    def download_binary(self):
        version = "v0.6.1"
        platform = sys.platform

        url = "https://github.com/kubernetes-sigs/kind/releases/download/{version}/kind-{platform}-amd64".format(
            version=version,
            platform=platform,
        )

        tmpf = tempfile.NamedTemporaryFile().name
        urllib.request.urlretrieve(url, tmpf)
        self.kind_binary = tmpf

        st = os.stat(tmpf)
        # make kind executable
        os.chmod(tmpf, st.st_mode | stat.S_IEXEC)
