#!/usr/bin/env python3

import boto3

class EKS:
    def __init__(self, name):
        self.name = name
        self.client = boto3.client('eks')

    def start(self):
        kwargs = {
            "name": self.name,
            "roleArn": "some-role",
            "resourcesVpcConfig":{
                'subnetIds': [
                    'string',
                ],
                'securityGroupIds': [
                    'string',
                ],
                'endpointPublicAccess': True,
                'endpointPrivateAccess': False,
            }
        }
        response = self.client.create_cluster(**kwargs)
        # TODO: this function needs to return when the cluster is
        # ready to operate

        # TODO: should I call 'aws eks update-kubeconfig --name example' ?
        # or maybe I can construct it myself by using data from the response?
