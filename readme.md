# KubeResources

Easy to start & delete Kubernetes clusters for local development.

## Example

Currently only "kind" provider is availabe, but I'm planning on adding more in
the near future. The next one to be added is EKS.

```python
# Kind() initialized a few things, most importantly, downloads kind if not present.
cluster = Kind()
# Returns the context of the newly created kind cluster
cluster.start()

while not cluster.is_ready():
    print("Waiting for kind to start")
    time.sleep(5)

print("Cluster is ready now")
print("This is the context config:", cluster.context_config())
```

After the Kubernetes cluster has been created, we can do whatever we want with
it.

```python
context = cluster.context_config()

from kubernetes import config
config.load_kube_config(context=context["context"])

v1 = client.CoreV1Api()
print("Listing pods with their IPs:")
ret = v1.list_pod_for_all_namespaces(watch=False)
for i in ret.items:
    print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))
```

And finally destroy it.

```python
cluster.delete()
```
